function [r,p,ci]=ICC(X)
% Intra-class correlations
% Input: 
% X = [n,2] with first and columns representing first and repeat measurements
% Output: 
% r - intra-class correlation coefficient
% p - p-value of intra-class correlation
% ci - 95% confidence interval of intra-class correlation
%
% (c) Joram Matthias Posma - Imperial College London - 2014-...
    
    conf=0.95; % confidence interval
    alp=1-conf; % significance level
    r0=0; % null hypothesis for correlation
    
    [ns,nr]=size(X);
    
    MSr=var(mean(X,2))*nr;
    MSw=sum(var(X,1,2)/ns*nr);
    
    r=(MSr-MSw)/(MSr+(nr-1)*MSw);
    Fvalue=MSr/MSw*((1-r0)/(1+(nr-1)*r0));
    df1=ns-1;
    df2=ns*(nr-1);
    
    p=fcdf(1/Fvalue,df2,df1);
    FL=Fvalue/finv(1-alp/2,df1,df2);
    FU=Fvalue*finv(1-alp/2,df2,df1);
    ci(1,1)=(FL-1)/(FL+(nr-1));
    ci(2,1)=(FU-1)/(FU+(nr-1));
    
end