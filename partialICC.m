function [r,p,ci]=partialICC(X,C1,C2)
% Partial intra-class correlations
% Input: 
% X = [n,2] with first and columns representing first and repeat measurements
% C1 = [n,c] with covariates to control X(:,1) for
% C2 = [n,c] with covariates to control X(:,2) for
% Output: 
% r - partial intra-class correlation coefficient
% p - p-value of partial intra-class correlation
% ci - 95% confidence interval of partial intra-class correlation
%
% (c) Joram Matthias Posma - Imperial College London - 2019-...

    conf=0.95; % confidence interval
    alp=1-conf; % significance level
    r0=0; % null hypothesis for correlation
    
    [ns,nr]=size(X);
    
    % mean-center
    mX=mean(X);
    X(:,1)=X(:,1)-mX(1);
    X(:,2)=X(:,2)-mX(2);
    C1=C1-repmat(mean(C1),ns,1);
    C2=C2-repmat(mean(C2),ns,1);
    
    % remove effect from confounders from X
    X(:,1)=X(:,1)-C1*((C1'*C1)\C1'*X(:,1));
    X(:,2)=X(:,2)-C2*((C2'*C2)\C2'*X(:,2));
	
    % map X back to original means, important in case the means were different
    X(:,1)=X(:,1)+mX(1);
    X(:,2)=X(:,2)+mX(2);

    MSr=var(mean(X,2))*nr;
    MSw=sum(var(X,1,2)/ns*nr);
    
    r=(MSr-MSw)/(MSr+(nr-1)*MSw);
    Fvalue=MSr/MSw*((1-r0)/(1+(nr-1)*r0));
    df1=ns-1;
    df2=ns*(nr-1);
    
    p=fcdf(1/Fvalue,df2,df1);
    FL=Fvalue/finv(1-alp/2,df1,df2);
    FU=Fvalue*finv(1-alp/2,df2,df1);
    ci(1,1)=(FL-1)/(FL+(nr-1));
    ci(2,1)=(FU-1)/(FU+(nr-1));
    
end