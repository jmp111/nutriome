function [acc,d]=ACC(cset)
% Adjusted Coefficient of Commonality (ACC)
% Input: 
% cset = [m,n] a charged binary set, where each element is {-1 0 1}
% Output: 
% acc = [n,n] matrix of the adjusted coefficient of commonality of columns in cset (similarity measure)
% d = acc distance (d = 1 - acc)
%
% (c) Joram Matthias Posma - Imperial College London - 2020-...
% From: Posma et al (2020) Nature Food (in press) (ref: NATFOOD-19110453A)
    
    n=size(cset,2);
    acc=eye(n);
    
    for k=2:n
        for j=1:(k-1)
            at=(sum(cset(:,k)==cset(:,j))-sum(cset(:,k)==(-1*cset(:,j))))/sum(sum(cset(:,[k j]).^2,2)~=0);
            acc(k,j)=at;
            acc(j,k)=at;
        end
    end
    
    d=1-acc;
    
end